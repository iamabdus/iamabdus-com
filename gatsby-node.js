const path = require('path')

exports.createPages = async ({ graphql, actions }) => {
  // Pages, Redirects
  const { createPage, createRedirect } = actions

  createRedirect({
    fromPath: '/blog/woocommerce/thumbnail-image-size-cart',
    toPath: '/blog/thumbnail-image-size-cart',
    isPermanent: true,
  })
  createRedirect({
    fromPath: '/blog/wordpress/filter-custom-posts-by-custom-field/',
    toPath: '/blog/filter-custom-posts-by-custom-field',
    isPermanent: true,
  })
  createRedirect({ fromPath: '/about', toPath: '/', isPermanent: true })

  const allPortfolioQueryResult = await graphql(
    `
      {
        allPrismicAllProject {
          nodes {
            data {
              projects {
                project {
                  uid
                }
              }
            }
          }
        }
      }
    `
  )
  // Handle errors
  if (allPortfolioQueryResult.errors) {
    reporter.panicOnBuild(`Error loading projects query.`)
    return
  }

  const portfolioPageTemplate = path.resolve(
    `src/templates/portfolio-single.js`
  )

  const projectsNode =
    allPortfolioQueryResult.data.allPrismicAllProject.nodes[0]
  const projects = projectsNode.data.projects

  projects.forEach(project => {
    createPage({
      path: '/portfolio/' + project.project.uid,
      component: portfolioPageTemplate,
      context: {
        slug: project.project.uid,
      },
    })
  })

  const allBlogQueryResult = await graphql(
    `
      {
        allPrismicBlog {
          nodes {
            data {
              blog_posts {
                blog_post {
                  uid
                }
              }
            }
          }
        }
      }
    `
  )
  // Handle errors
  if (allBlogQueryResult.errors) {
    reporter.panicOnBuild(`Error loading blog query.`)
    return
  }

  const blogPageTemplate = path.resolve(`src/templates/blog-single.js`)

  const blogsNode = allBlogQueryResult.data.allPrismicBlog.nodes[0]
  const blogs = blogsNode.data.blog_posts

  blogs.forEach(blog => {
    createPage({
      path: '/blog/' + blog.blog_post.uid,
      component: blogPageTemplate,
      context: {
        slug: blog.blog_post.uid,
      },
    })
  })
}
