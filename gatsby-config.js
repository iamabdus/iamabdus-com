
module.exports = {
  siteMetadata: {
    title: 'Freelance UI UX Designer & Front-end Developer | Abdus Salam',
  },
  plugins: [
    'gatsby-plugin-styled-components',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sharp', 
    'gatsby-transformer-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        background_color: '#13151B',
        theme_color: '#E44026',
        display: 'minimal-ui',
        icon: 'src/images/iamabdus.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-16298035-1",
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Delays sending pageview hits on route update (in milliseconds)
      },
    },
    'gatsby-plugin-offline',
    {
      resolve: 'gatsby-source-prismic',
      options: {
        repositoryName: 'iamabduscom',
        accessToken:
          'MC5XLUtUb3hFQUFDRUFlSHV2.77-977-977-977-977-977-977-9ARXvv71YBnzvv70OYe-_ve-_vXrvv73vv71oFu-_vV3vv70077-9aTMjJQ',
      },
    },
    'gatsby-plugin-meta-redirect'
  ],
}
