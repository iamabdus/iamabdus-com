export default [
  { label: '$500', value: '500' },
  { label: '$1,000', value: '1000' },
  { label: '$3,000', value: '3000' },
  { label: '$5,000', value: '5000' },
  { label: '$10,000', value: '10000' },
  { label: 'I Want to Discuss', value: 'Want to discuss' },
]
