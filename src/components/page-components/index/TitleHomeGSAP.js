import React, { Component } from 'react'
import TitleHomeSingle from './TitleHomeSingle'

const titles = ['Web Devoloper', 'Designer', 'UI Devoloper']
// const titles = ['Web Devoloper']

class TitleHome extends Component {
  state = {
    title: titles[0],
    index: 0,
    nextItemDelay: 0,
  }

  getTime = time => {
    this.setState({
      index: this.state.index + 1,
      nextItemDelay: time,
    })
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevState.index !== this.state.index) {
  //     setTimeout(() => {
  //       this.setState({
  //         title: titles[this.state.index],
  //       })
  //     }, 5000)
  //   }
  // }

  render() {
    const { delay } = this.props
    const { title, index } = this.state
    return <TitleHomeSingle delay={delay} />
  }
}

export default TitleHome
