import React, { Component } from 'react'
import styled from 'styled-components'
import { TimelineMax } from 'gsap/TweenMax'
import { TransitionGroup } from 'react-transition-group'
import media from '../../../utility/media'
import theme from '../../../utility/theme'
import { common } from '../../../utility/common'
import TitleChars from './TitleChars'

const StyledTitle = styled.div`
  font-family: ${theme.fontTitle};
  color: #fff;
  font-size: 36px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 2.04;
  letter-spacing: 3.6px;
  @media (min-width: ${media.md}) {
    font-size: 60px;
    line-height: 1.83;
    letter-spacing: 6px;
  }
  @media (min-width: ${media.lg}) {
    font-size: 105px;
    line-height: 1.04;
    letter-spacing: 10.5px;
  }
`

class TitleHomeSingle extends Component {
  state = {
    text: null,
    reverse: false,
  }

  constructor(props) {
    super()
    this.textTween = null
    this.totaltime = 0
    this.isLastItem = false
    // this.charItems = []
  }

  componentDidMount() {
    // this.animateText(this.charItems)
    setTimeout(() => {
      this.time = this.setState(state => {
        return {
          text: 'TAF Tech',
          animation: true,
        }
      })
    }, this.props.delay * 1000)
  }

  // componentDidUpdate(prevProps, prevState) {
  // if (prevState.text !== this.state.text) {
  //   if (this.state.animation === true) {
  //     this.animateText(this.charItems)
  //     this.setState(state => {
  //       return {
  //         animation: false,
  //       }
  //     })
  //   }
  // }
  // }

  changeText = () => {
    this.setState({
      text: 'Hello World',
      animation: true,
    })
  }

  animateText = chars => {
    if (this.state.text === 'TAF Tech') {
      return
    }

    this.textTween = new TimelineMax().staggerFrom(
      chars,
      0.3,
      { autoAlpha: 0, ease: common.easing.easeOut, delay: this.props.delay },
      0.2
    )

    /**
     ********  Total Time Calculation  ********
     * Multiply with 2 for 2 times animation (normal and reverse) then Add 0.3 for 3000 from setTimeOut delay and subtract current delay, because next don't take primary delay
     *
     **/

    //  const timeDelay = this.textTween.totalTime() * 2 + 0.3

    // this.props.getTime(10)

    this.timerTextTween = setTimeout(() => {
      this.textTween.reverse()
    }, 3000)
  }

  clearAll = () => {
    // this.textTween.kill()
    // clearTimeout(this.timerTextTween)
    clearTimeout(this.time)
  }

  componentWillUnmount() {
    // this.clearAll()
  }

  getTime = time => {
    this.totaltime = this.totaltime + time
    let print = this.isLastItem ? this.totaltime : null
    console.log(print)
  }

  isLastItem = islast => {
    console.log(islast === true)
  }

  render() {
    const { text, show } = this.state

    return text ? (
      <StyledTitle>
        <TransitionGroup>
          {text.split('').map((item, index) => {

            this.isLastItem = text.length - 1 === index ? true : false

            return (
              <TitleChars
                key={index}
                index={index}
                item={item}
                isLastItem = {this.isLastItem}
                getTotalTime={this.getTime}
              />
            )
          })}
        </TransitionGroup>
      </StyledTitle>
    ) : null
  }
}

export default TitleHomeSingle
