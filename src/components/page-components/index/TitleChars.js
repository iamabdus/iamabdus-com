import React, { Component } from 'react'
import styled from 'styled-components'
import { TweenMax } from 'gsap/all'
import { Transition } from 'react-transition-group'

//Important for GSAP animation
const StyledSpan = styled.span`
  position: relative;
  display: inline-block;
  -webkit-font-smoothing: antialiased;
`

class TitleChars extends Component {
  constructor(props) {
    super(props)
    this.tween = null
    this.totalTime = null
  }

  render() {
    const { in: show, item, index, getTotalTime } = this.props
    return item === ' ' ? (
      <Transition
        timeout={1000}
        mountOnEnter
        unmountOnExit
        appear
        in={show}
        addEndListener={(node, done) => {
          this.tween = TweenMax.from(node, 0.3, {
            autoAlpha: show ? 0 : 1,
            onComplete: done,
            delay: index * 0.2,
          })
          getTotalTime(this.tween.delay() + this.tween.duration())
        }}
      >
        <StyledSpan>&nbsp;</StyledSpan>
      </Transition>
    ) : (
      <Transition
        timeout={1000}
        mountOnEnter
        unmountOnExit
        appear
        in={show}
        addEndListener={(node, done) => {
          this.tween = TweenMax.from(node, 0.3, {
            autoAlpha: show ? 0 : 1,
            onComplete: done,
            delay: index * 0.3,
          })
          getTotalTime(this.tween.delay() + this.tween.duration())
        }}
      >
        <StyledSpan>{item}</StyledSpan>
      </Transition>
    )
  }
}

export default TitleChars
